import axios from "axios";
import {API} from '../store/actions/types'
const ApiMiddleware = ({ dispatch, getState }) => next => action => {
    if (action.type !== API)
        return;
    const {
        url,
        method,
        data,
        accessToken,
        headersOverride,
        onSuccess,
        onFailure,
        label,
        headers
    } = action.payload;

    const dataOrParams = ["GET"].includes(method) ? "params" : "data";
    axios.defaults.baseURL = "";
    axios.defaults.headers.common["Content-Type"] = "application/json";
    axios.defaults.headers.common["Authorization"] = `Bearer${accessToken}`;
   // console.log("url", url, "method", method, "headers", headers, "headersOverride", headersOverride, "data", data)
    axios
        .request({
            url,
            method,
            headers: headersOverride ? headersOverride : headers,
            [dataOrParams]: data,
        })
        .then(({ data }) => {

            if (data.sc && data.result != null) {
                // console.log("resopnse= " + JSON.stringify(data))
                dispatch(onSuccess(data.result));  //-> SUCCESS callback
            } else {
                this.handleFailure(data)
            }
        })
        .catch(error => {
            if (error.response) {
                dispatch(onFailure(error.response.data.error)); //-> FAILURE callback
            }
        })
        .finally(() => {

        });
};


export default ApiMiddleware;