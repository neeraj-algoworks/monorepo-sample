import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';
import { connect, } from 'react-redux';

import { updateUsersList } from '../shared/store/actions/home';
import { loginViaEmailPass } from '../shared/store/actions/login';

interface Props {
  updateUsersList: (s: string) => void
  loginViaEmailPass: (email: string, pass: string) => Promise<any>
  users: string[]
}

//type Props={}
class UserList extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }
  componentDidMount() {
    this.props.updateUsersList('Ram');
    this.props.loginViaEmailPass("sumitsahu28dev@gmail.com", "12345678")
    .then((res: any) => {
        console.log("SUCESS", res)
      }).catch((err: any) => {
        console.log("FAILURE", err)
      })
  }
  renderItem = ({ item }: { item: string }) => (
    <View style={styles.item}>
      <Text>{item}</Text>
    </View>
  );
  render() {
    const { users } = this.props;
    return (
      <FlatList
        style={styles.container}
        data={users}
        renderItem={this.renderItem}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  item: {
    padding: 16,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc'
  }
});


const mapStateToProps = (state: any) => ({
  users: state.users
});
const mapDispatchToProps = (dispatch: any) => {
  return {
    updateUsersList: (data: string) => dispatch(updateUsersList(data)),
    loginViaEmailPass: (email: string, pass: string) => dispatch(loginViaEmailPass(email, pass))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserList);
