import {combineReducers} from 'redux';
import dashboardReducer from './home';
export default combineReducers({
   dashboard: dashboardReducer
})