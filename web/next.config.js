// const path = require('path');

// const mainConfig = {
//     webpack: (config, { defaultLoaders }) => {
//         console.log('Next config', config, defaultLoaders)
//         config.resolve = {
//             ...config.resolve,
//             // alias: {
//             //     ...config.resolve.alias,
//             //     'react-native': path.join(__dirname, 'node_modules', 'react-native-web'),
//             // }
//             modules: [
//                 ...config.resolve.modules,
//                 path.resolve(__dirname, 'node_modules'),
//             ],
//             symlinks: false,
//         }

//         config.modules.rules.push({
//             test: /\.+(js|jsx)$/,
//             use: defaultLoaders.babel,
//             include: [path.resolve(__dirname, '..', 'shared')],
//         })

//         return config;
//     },
// }

// module.exports = mainConfig;

exports.modifyWebpackConfig = ({ config, _stage }) => {
    console.log("*********config._config.context: ", config._config.context)
  return config.merge({
    resolve: {
      alias: {
        shared: path.resolve(config._config.context, '../shared'),
      },
    },
  })
}