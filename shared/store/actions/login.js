import { 
    USER_LOGIN
} from './types';
import ApiActionCreator from '../../ApiManager/ApiActionCreator';

export const loginViaEmailPass = (emailId, userPass) =>
    (dispatch) => {
        return new Promise((resolve, reject) => {
            const parameters = {
                email: emailId,
                password: userPass,
                device_token: "qwerty",
                device_type: "1"
            }
            const loginUrl = "http://52.206.130.241/geopay/api/v0/user/login"
            //returns a funtion, not an action object
            dispatch(ApiActionCreator.getInstance().apiActionCall({
                url: loginUrl,
                method: "POST",
                onSuccess: async (data) => {
                    resolve((data));
                },
                onFailure: (error) => {
                    reject(error)
                },
                label: USER_LOGIN,
                data: parameters,
                headersOverride: {
                    app_language: 'en',
                    app_version: '1.0'
                }
            }));


        });
    };