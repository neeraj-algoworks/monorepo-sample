import {welcome} from "../../shared/Utility/message";
import {loadInitialData} from "../../shared/store/actions/home";
import { connect } from "react-redux";
 function Index(props) {
    return (
      <div>
        <h1 style={{textAlign:'center'}}>{welcome}</h1>
        <h3 style={{textAlign:'center'}}>Value coming from store: {props.user.name}</h3>
      </div>
    );
  }
  const mapStateToPropsCommon = state => {
    return {
      user: state.dashboard.user,
    };
  };
  
  const mapDispatchToPropsCommon = dispatch => {
    return {
      loadInitialData: data => dispatch(loadInitialData(data)),
  
    }
  }
  

  export default(
    connect(
      mapStateToPropsCommon,
      {mapDispatchToPropsCommon}
    )(Index));