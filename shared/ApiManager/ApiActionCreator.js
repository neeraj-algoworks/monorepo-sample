import { API } from '../store/actions/types';

let instance = null;

class ApiActionCreator {
    constructor() {
        if (!instance) {
            this.instance = this;
        }
        return instance;
    }

    static getInstance = () => {
        return new ApiActionCreator();
    };


    //--------------- API REQUESTING ------------------//

    // METHOD: TO RETURN ACTION FOR API_MIDDLEWARE FOR API CALLS
    apiActionCall = ({
        url = "",
        method = "GET",
        data = null,
        accessToken = null,
        onSuccess = () => { },
        onFailure = () => { },
        label = "",
        headersOverride = null
    }) => {
        //return action object of type='API'
        return {
            type: API,
            payload: {
                url,
                method,
                data,
                accessToken,
                onSuccess,
                onFailure,
                label,
                headersOverride
            }
        };
    };


}

export default ApiActionCreator;