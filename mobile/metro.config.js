const path = require('path');

const config = {
    /*
     Ensure any imports inside the shared folder resolve to the local node_modules folder
     */
    resolver: {
        extraNodeModules: new Proxy({}, {
            get: (target, name) => path.join(process.cwd(), `node_modules/${name}`),
        })
    },
    /**
     * Add our workspace roots so that react native cn find the source code for the included packages
     * in the monorepo
     */
    projectRoot: path.resolve(__dirname),
    watchFolders: [
        path.resolve(__dirname, '../shared'),
    ],
    transformer: {
        getTransformOptions: async () => ({
          transform: {
            experimentalImportSupport: false,
            inlineRequires: false,
          },
        }),
      },
};

module.exports = config;