import {
    FETCH_INITIAL_DATA,
    UPDATE_USERS_LIST
}  from './../actions/types';

const INITIAL_STATE = {
    user: {
        name: 'tester',
        email: 'test@gmail.com'
    },
    users: [
        "John",
        "Mike",
        "Rohn",
        "Don",
        "Jam"
    ]
};

export default function (state = INITIAL_STATE , action)
{
    switch(action.type){
        case FETCH_INITIAL_DATA : 
        return {
            ...state
        }
        case UPDATE_USERS_LIST : { 
        return {
            ...state,
            users: [...state.users, action.payload]
        }
    }

        default :
        return{
            ...state
        };
    }
}