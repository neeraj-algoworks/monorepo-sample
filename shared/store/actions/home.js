import { 
    FETCH_INITIAL_DATA ,
    UPDATE_USERS_LIST
} from './types';

export const loadInitialData = () => {
    return dispatch => {
        dispatch(
            {
              type: FETCH_INITIAL_DATA
            }
        )
    }
}

export const updateUsersList = (name) => {
    return dispatch => {
        dispatch(
            {
              type: UPDATE_USERS_LIST,
              payload: name
            }
        )
    }
}

// export {
//     loadInitialData,
//     getUsersList
// }
