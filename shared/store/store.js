import {createStore, applyMiddleware} from 'redux';
import reduxThunk from 'redux-thunk';
import ApiMiddleware from '../ApiManager/ApiMiddleware' // axios 
import reducers from './reducers/index';


const middleware = applyMiddleware(reduxThunk,ApiMiddleware);
 export default createStore(reducers, middleware);