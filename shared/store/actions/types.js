export const FETCH_INITIAL_DATA = "fetch_initial_data";
export const UPDATE_USERS_LIST = "update_users_list";
export const USER_LOGIN = "user_login";
export const API = "Api";